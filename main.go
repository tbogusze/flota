package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"
)

func createGame(game *Game, client *http.Client) (string, error) {
	j, err := toJSON(game)
	if err != nil {
		return "", err
	}

	url, _ := url.JoinPath(baseURL, "game")
	resp, err := sendRequest("POST", url, "", []byte(string(j)))
	if err != nil {
		return "", err
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("Error creating game: %s", resp.Status)
	}

	token := resp.Header.Get("X-Auth-Token")
	if token == "" {
		return "", fmt.Errorf("No X-Auth-Token in response header.")
	}

	return token, nil
}

func waitForGameStart(client *http.Client, token, url string) {
	for {
		time.Sleep(1 * time.Second)
		gameStatus, err := getGameStatus(url, token)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Game status: %v\n", gameStatus)

		if gameStatus.Status != "waiting" && gameStatus.Status != "waiting_wpbot" {
			break
		}
	}
}

func main() {
	nickname := getNickname()
	description := getDescription()
	for {
		ships := getShips()
		playWithBot := getBotChoice()
		targetNick := getOpponentChoice(playWithBot)

		game := &Game{
			Coords:     ships,
			Desc:       description,
			Nick:       nickname,
			TargetNick: targetNick,
			WpBot:      playWithBot,
		}

		client := &http.Client{}
		token, err := createGame(game, client)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(token)

		url, _ := url.JoinPath(baseURL, "game")
		waitForGameStart(client, token, url)

		descs, err := getDescriptions(url, token)
		if err != nil {
			log.Fatal(err)
		}

		boardsURL := fmt.Sprintf("%s/game/board", baseURL)
		boards, err := getBoard(boardsURL, token)
		if err != nil {
			log.Println(err)
		}
		log.Println(boards)

		displayBoard(boards, token, descs, client, url, nickname, descs.Opponent)

		if !startNewGame() {
			return
		}
	}
}

// Helper functions

func getLine() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return strings.TrimSpace(scanner.Text())
}

func getNickname() string {
	var nickname string
	for {
		fmt.Print("Nickname: ")
		nickname = getLine()
		match, err := regexp.MatchString("^[a-zA-Z0-9]*$", nickname)
		if err != nil || !match {
			fmt.Println("Nickname must be alphanumeric.")
			continue
		}
		if len(nickname) >= 2 && len(nickname) <= 10 {
			break
		}
		fmt.Println("Nickname must be between 2 and 10 characters.")
	}
	return nickname
}

func getDescription() string {
	fmt.Print("Description: ")
	return getLine()
}

func getChoice(text string, option1, option2 string) string {
	var ans string
	for {
		fmt.Printf("%s (%s/%s): ", text, option1, option2)
		fmt.Scanln(&ans)
		ans = strings.TrimSpace(ans)
		if ans == option1 || ans == option2 {
			break
		}
		fmt.Printf("Input must be %s or %s.\n", option1, option2)
	}
	return ans
}

func getYesNo(text string) string {
	return getChoice(text, "y", "n")
}

func getBotChoice() bool {
	return getYesNo("Would you like to play with the bot?") == "y"
}

func getOpponentChoice(wpbot bool) string {
	if !wpbot {
		wpbotStr := getChoice("Would you like to wait or choose your opponent?", "w", "o")

		if wpbotStr == "w" {
			// Wait for opponent
			fmt.Println("Waiting for opponent...")
			return ""
		} else {
			// Choose opponent
			fmt.Println("Choosing opponent...")
			targetNick := "r"
			for targetNick == "r" {
				opponents, err := getOpponents(baseURL)
				if err != nil {
					log.Fatal(err)
				}
				// Print them out
				for i, opponent := range opponents {
					fmt.Printf("%d: %s\n", i, opponent.Nick)
				}
				// Let the user choose one
				fmt.Print("Choose your opponent (or press r to refresh the list): ")
				fmt.Scanln(&targetNick)
			}
			return targetNick
		}
	}
	return ""
}

func getShips() []string {
	var ships []string
	userChoice := getYesNo("Would you like to choose your own ships?")

	if userChoice == "y" {
		ships = defineShips()
	}

	return ships
}

func startNewGame() bool {
	return getYesNo("Would you like to start another game?") == "y"
}
