package main

import "encoding/json"

type Game struct {
	Coords     []string `json:"coords"`
	Desc       string   `json:"desc"`
	Nick       string   `json:"nick"`
	TargetNick string   `json:"target_nick"`
	WpBot      bool     `json:"wpbot"`
}

type GameStatus struct {
	Desc           string   `json:"desc"`
	Status         string   `json:"game_status"`
	LastGameStatus string   `json:"last_game_status"`
	Nick           string   `json:"nick"`
	OppDesc        string   `json:"opp_desc"`
	OppShots       []string `json:"opp_shots"`
	Opponent       string   `json:"opponent"`
	ShouldFire     bool     `json:"should_fire"`
	Timer          int      `json:"timer"`
}

type Descriptions struct {
	Nick           string `json:"nick"`
	Desc           string `json:"desc"`
	GameStatus     string `json:"game_status"`
	LastGameStatus string `json:"last_game_status"`
	Opponent       string `json:"opponent"`
	OppDesc        string `json:"opp_desc"`
	ShouldFire     bool   `json:"should_fire"`
	Timer          int    `json:"timer"`
}

type Player struct {
	Nick       string `json:"nick"`
	GameStatus string `json:"game_status"`
}

type Stats struct {
	Nick   string `json:"nick"`
	Games  int    `json:"games"`
	Wins   int    `json:"wins"`
	Rank   int    `json:"rank"`
	Points int    `json:"points"`
}

func toJSON(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

// Implement enum from scratch since Golang is stupid
type GameOver int

const (
	Nope GameOver = iota
	Win
	Lose
)

func (s GameOver) String() string {
	switch s {
	case Nope:
		return "nope"
	case Win:
		return "win"
	case Lose:
		return "lose"
	}
	return "unknown"
}
