package main

import (
	"encoding/json"
	"fmt"
	"net/url"
)

func getOpponents(baseURL string) ([]Player, error) {
	url, _ := url.JoinPath(baseURL, "game/list")
	resp, err := sendRequest("GET", url, "", nil)
	if err != nil {
		return []Player{}, err
	}
	defer resp.Body.Close()

	var opponents []Player
	err = json.NewDecoder(resp.Body).Decode(&opponents)

	if err != nil {
		return []Player{}, err
	}

	return opponents, nil
}

func getPlayerStats(baseURL, player string) (Stats, error) {
	url, _ := url.JoinPath(baseURL, "stats", player)
	fmt.Println(url)
	resp, err := sendRequest("GET", url, "", nil)
	if err != nil {
		return Stats{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 404 {
		return Stats{Nick: player, Games: 0, Wins: 0, Rank: 0, Points: 0}, nil
	}

	var stats map[string]Stats
	err = json.NewDecoder(resp.Body).Decode(&stats)

	if err != nil {
		return Stats{}, err
	}

	if stats, ok := stats["stats"]; ok {
		return stats, nil
	} else {
		return Stats{}, fmt.Errorf("No stats for %s", player)
	}
}
