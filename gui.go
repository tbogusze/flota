package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	gui "github.com/grupawp/warships-gui/v2"
)

func fieldToInts(field string) (int, int) {
	x := int(field[0] - 'A')
	multiplier, y := 1, 0
	for i := len(field) - 1; i > 0; i-- {
		y += int(field[i]-'0') * multiplier
		multiplier *= 10
	}
	return x, y - 1
}

func updateBoard(x, y int, board *gui.Board, states *[10][10]gui.State, hit bool) {
	cumulative := states[x][y] == gui.Miss || states[x][y] == gui.Hit
	if cumulative {
		return
	}

	if states[x][y] == gui.Ship {
		hit = true
	}

	if hit {
		states[x][y] = gui.Hit
	} else {
		states[x][y] = gui.Miss
	}
	board.SetStates(*states)
}

func markHitField(x, y int, board *[10][10]gui.State) {
	if x < 0 || x >= 10 || y < 0 || y >= 10 || board[x][y] != gui.Ship {
		return
	}

	deltaMoves := [][]int{
		{-1, -1}, {-1, 0}, {-1, 1},
		{0, -1}, {0, 1},
		{1, -1}, {1, 0}, {1, 1},
	}

	for _, item := range deltaMoves {
		newX, newY := x+item[0], y+item[1]
		if newX < 0 || newX >= 10 || newY < 0 || newY >= 10 {
			continue
		}
		if board[newX][newY] != gui.Miss && board[newX][newY] != gui.Hit && board[newX][newY] != gui.Ship {
			board[newX][newY] = gui.Miss
		}
	}
}

func sinkShip(x, y int, board *gui.Board, states *[10][10]gui.State) int {
	if x < 0 || x >= 10 || y < 0 || y >= 10 || states[x][y] != gui.Hit {
		return 0
	}
	length := 0
	states[x][y] = gui.Ship

	length += sinkShip(x-1, y, board, states) + sinkShip(x+1, y, board, states) + sinkShip(x, y-1, board, states) + sinkShip(x, y+1, board, states)

	markHitField(x, y, states)
	board.SetStates(*states)

	return 1 + length
}

func contains(slice []string, item string) bool {
	for _, v := range slice {
		if v == item {
			return true
		}
	}
	return false
}

func updateGameOver(gameStatus *GameStatus, isGameOver *GameOver) {
	if gameStatus.Status == "ended" {
		switch gameStatus.LastGameStatus {
		case "no_game":
			fmt.Println("Unhandled error: no game")
			*isGameOver = Nope
		case "win":
			*isGameOver = Win
		case "lose":
			*isGameOver = Lose
		}
	}
}

func waitForYourTurn(url, token string, board *gui.Board, txt *gui.Text, states *[10][10]gui.State) (GameOver, error) {
	gameOver := Nope
	enemyHits := []string{}
	status := GameStatus{ShouldFire: false}
	for !status.ShouldFire {
		gameStatus, err := getGameStatus(url, token)
		status = gameStatus
		if err != nil {
			return Nope, err
		}
		enemyHits = gameStatus.OppShots
		for _, enemyHit := range enemyHits {
			xEnemyShot, yEnemyShot := fieldToInts(enemyHit)
			updateBoard(xEnemyShot, yEnemyShot, board, states, false)
		}
		updateGameOver(&gameStatus, &gameOver)

		if gameOver != Nope {
			break
		}

		time.Sleep(500 * time.Millisecond)
	}
	return gameOver, nil
}

func handleGameOver(isGameOver GameOver, txt *gui.Text, ui *gui.GUI) bool {
	if isGameOver != Nope {
		txt.SetText(fmt.Sprintf("Game Over: %s", isGameOver.String()))
		if isGameOver == Win {
			txt.SetBgColor(gui.Green)
		} else {
			txt.SetBgColor(gui.Red)
		}
		ui.Draw(txt)
		time.Sleep(10 * time.Second)
		return true
	}
	return false
}

// Function to check if there are ships around
func checkShipsAround(x, y int, states [10][10]gui.State) int {
	states[x][y] = gui.Ship
	return checkShipsAroundHelper(x, y, states, &[]int{}, &[]int{}) - 1
}

func checkShipsAroundHelper(x, y int, states [10][10]gui.State, xShips, yShips *[]int) int {
	numShips := 0
	if x < 0 || x >= 10 || y < 0 || y >= 10 {
		return 0
	}
	for i := 0; i < len(*xShips); i++ {
		if (*xShips)[i] == x && (*yShips)[i] == y {
			return 0
		}
	}
	if states[x][y] == gui.Ship {
		numShips++
		*xShips = append(*xShips, x)
		*yShips = append(*yShips, y)
		numShips += checkShipsAroundHelper(x-1, y, states, xShips, yShips) +
			checkShipsAroundHelper(x+1, y, states, xShips, yShips) +
			checkShipsAroundHelper(x, y-1, states, xShips, yShips) +
			checkShipsAroundHelper(x, y+1, states, xShips, yShips)
	}
	return numShips
}

func checkLegality(x, y int, states [10][10]gui.State, length int) bool {
	// check top left corner, bottom right corner, and diagonal
	checks := []int{
		-1, 1, -1, -1, 1, -1, 1, 1,
	}
	for i := 0; i < 4; i++ {
		xCheck := x + checks[i*2]
		yCheck := y + checks[i*2+1]
		if xCheck < 0 || xCheck >= 10 || yCheck < 0 || yCheck >= 10 {
			continue
		}
		if states[xCheck][yCheck] == gui.Ship {
			if length > 1 && (states[xCheck][y] == gui.Ship || states[x][yCheck] == gui.Ship) {
				continue
			}
			return false
		}
	}
	return true
}

// Function to define the ships chosen by the user graphically
func defineShips() []string {
	// Initialize board display
	ui := gui.NewGUI(true)
	board := gui.NewBoard(2, 4, nil)
	ui.Draw(board)

	// Set initial board state
	states := [10][10]gui.State{}

	// Display instructions
	txt := gui.NewText(2, 1, "Click to place the ships!", nil)
	countText := gui.NewText(2, 28, "", nil)
	ui.Draw(txt)
	ui.Draw(countText)

	// Counter to track ships placed
	placedShips := make(map[int]int)
	maxAllowedShips := map[int]int{
		4: 1,
		3: 2,
		2: 3,
		1: 4,
	}

	// Set up context for the board
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Get user inputs
	go func() {
		var xSelected, ySelected int

		txt.SetText("Choose your ships")
		txt.SetBgColor(gui.White)

		// while sum of values of placedShips map is less than 10
		for placedShips[1]+placedShips[2]+placedShips[3]+placedShips[4] < 10 {
			char := board.Listen(context.TODO())
			xSelected, ySelected = fieldToInts(char)

			// Check grid isn't empty
			switch states[xSelected][ySelected] {
			case gui.Ship:
				txt.SetText("Coordinate not empty! Choose another place!")
				txt.SetBgColor(gui.Red)
			default:
				// Check if there are ships around
				shipLength := checkShipsAround(xSelected, ySelected, states) + 1
				if !checkLegality(xSelected, ySelected, states, shipLength) {
					txt.SetText("Can't place a ship here! Choose another place!")
					txt.SetBgColor(gui.Red)
					break
				}
				if shipLength > 4 {
					txt.SetText("Ship is too long! Choose another one!")
					txt.SetBgColor(gui.Red)
					break
				}
				if placedShips[shipLength] < maxAllowedShips[shipLength] {
					placedShips[shipLength]++
					if shipLength > 1 {
						placedShips[shipLength-1]--
					}
				} else {
					txt.SetText("Too many ships of this length! Choose another place!")
					txt.SetBgColor(gui.Red)
					break
				}
				states[xSelected][ySelected] = gui.Ship
				board.SetStates(states)
				countText.SetText(fmt.Sprintf("Four ship: %d | Three ship: %d | Two ship: %d | One ship: %d.", placedShips[4], placedShips[3], placedShips[2], placedShips[1]))

				txt.SetText("Choose your ships")
				txt.SetBgColor(gui.White)
			}
		}
		txt.SetText("Ships placed!")
		txt.SetBgColor(gui.White)
		time.Sleep(2 * time.Second)
		cancel()
	}()

	ui.Start(ctx, nil)

	// Set up slice to return
	selected := make([]string, 0)

	// add states to selected
	for x := 0; x < 10; x++ {
		for y := 0; y < 10; y++ {
			if states[x][y] == gui.Ship {
				selected = append(selected, fmt.Sprintf("%c%d", 'A'+x, y+1))
			}
		}
	}

	// Return with ships placed
	return selected
}

func updateStats(nickname string, text *gui.Text, ui *gui.GUI) {
	stats, err := getPlayerStats(baseURL, nickname)
	if err != nil {
		ui.Log("Couldn't get player stats for player: " + nickname)
	}
	rankText := fmt.Sprintf("Rank: %d", stats.Rank)
	if stats.Rank == 0 {
		rankText = "No rank"
	}
	text.SetText(fmt.Sprintf("%s stats | %s | Wins: %d | Losses: %d", nickname, rankText, stats.Wins, stats.Games-stats.Wins))
}

func updateLeftShips(text *gui.Text, shipsLeft map[int]int) {
	text.SetText(fmt.Sprintf("Ships left | Four ship: %d | Three ship: %d | Two ship: %d | One ship: %d",
		shipsLeft[4], shipsLeft[3], shipsLeft[2], shipsLeft[1]))
}

func displayBoard(boards Board, token string, descriptions Descriptions, client *http.Client, url string, ourName, enemyName string) {
	// Set initial board
	ourStates := [10][10]gui.State{}
	for _, ship := range boards.Board {
		x, y := fieldToInts(ship)
		ourStates[x][y] = gui.Ship
	}
	enemyStates := [10][10]gui.State{}

	// Init board to display
	ui := gui.NewGUI(true)
	ourBoard := gui.NewBoard(2, 4, nil)
	enemyBoard := gui.NewBoard(50, 4, nil)
	ui.Draw(ourBoard)
	ui.Draw(enemyBoard)
	ourBoard.SetStates(ourStates)
	enemyBoard.SetStates(enemyStates)

	// Mark your and enemy's board
	ourBoardText := gui.NewText(19, 26, "Your board", nil)
	enemyBoardText := gui.NewText(67, 26, "Enemy board", nil)
	ui.Draw(ourBoardText)
	ui.Draw(enemyBoardText)

	// Initialize turn texts
	txt := gui.NewText(2, 1, "Wait for your turn...", nil)
	ui.Draw(txt)
	timerTxt := gui.NewText(100, 3, "", nil)
	ui.Draw(timerTxt)

	// Initialize description texts
	myDescTxt := gui.NewText(2, 28, descriptions.Desc, nil)
	enemyDescTxt := gui.NewText(50, 28, descriptions.OppDesc, nil)
	ui.Draw(myDescTxt)
	ui.Draw(enemyDescTxt)

	// Tracking total shot accuracy
	accuracyTxt := gui.NewText(100, 1, "Shot accuracy: ", nil)
	ui.Draw(accuracyTxt)
	totalShots := 0
	totalHits := 0

	ourStatsTxt := gui.NewText(100, 5, "", nil)
	enemyStatsTxt := gui.NewText(100, 7, "", nil)
	ui.Draw(ourStatsTxt)
	ui.Draw(enemyStatsTxt)

	updateStats(ourName, ourStatsTxt, ui)
	updateStats(enemyName, enemyStatsTxt, ui)

	// Tracking ships left
	shipsLeftTxt := gui.NewText(100, 9, "", nil)
	shipsLeft := map[int]int{
		4: 1,
		3: 2,
		2: 3,
		1: 4,
	}
	updateLeftShips(shipsLeftTxt, shipsLeft)
	ui.Draw(shipsLeftTxt)

	// Game tutorial
	tutorialTxt := gui.NewText(100, 11, "Click a field on the enemy board to shoot.", nil)
	ui.Draw(tutorialTxt)
	tutorialTxt2 := gui.NewText(100, 12, "You will see the result right away.", nil)
	ui.Draw(tutorialTxt2)
	tutorialTxt3 := gui.NewText(100, 13, "You can also tracck your shot accuracy above.", nil)
	ui.Draw(tutorialTxt3)

	// Our board fields explanation
	ourFieldsTxt := gui.NewText(100, 15, "Your board legend:", nil)
	ui.Draw(ourFieldsTxt)
	emptyShip := gui.NewText(100, 16, " ~ ", nil)
	emptyShip.SetBgColor(gui.Blue)
	ui.Draw(emptyShip)
	emptyShipDesc := gui.NewText(103, 16, " - No ship there, not hit yet", nil)
	ui.Draw(emptyShipDesc)
	normalShip := gui.NewText(100, 17, " S ", nil)
	normalShip.SetBgColor(gui.Green)
	ui.Draw(normalShip)
	normalShipDesc := gui.NewText(103, 17, " - Normal ship, not hit yet", nil)
	ui.Draw(normalShipDesc)
	hitShip := gui.NewText(100, 18, " H ", nil)
	hitShip.SetBgColor(gui.Red)
	ui.Draw(hitShip)
	hitShipDesc := gui.NewText(103, 18, " - Hit ship", nil)
	ui.Draw(hitShipDesc)
	missedShip := gui.NewText(100, 19, " M ", nil)
	missedShip.SetBgColor(gui.Grey)
	ui.Draw(missedShip)
	missedShipDesc := gui.NewText(103, 19, " - Missed, no ship there", nil)
	ui.Draw(missedShipDesc)

	// Enemy board fields explanation
	enemyFieldsTxt := gui.NewText(100, 21, "Enemy board legend:", nil)
	ui.Draw(enemyFieldsTxt)
	unknownShip := gui.NewText(100, 22, " ~ ", nil)
	unknownShip.SetBgColor(gui.Blue)
	ui.Draw(unknownShip)
	unknownShipDesc := gui.NewText(103, 22, " - Not hit yet, status unknown", nil)
	ui.Draw(unknownShipDesc)
	hitShip2 := gui.NewText(100, 23, " H ", nil)
	hitShip2.SetBgColor(gui.Red)
	ui.Draw(hitShip2)
	hitShipDesc2 := gui.NewText(103, 23, " - Hit ship", nil)
	ui.Draw(hitShipDesc2)
	sunkShip := gui.NewText(100, 24, " S ", nil)
	sunkShip.SetBgColor(gui.Green)
	ui.Draw(sunkShip)
	sunkShipDesc := gui.NewText(103, 24, " - Sunk ship", nil)
	ui.Draw(sunkShipDesc)
	missedShip2 := gui.NewText(100, 25, " M ", nil)
	missedShip2.SetBgColor(gui.Grey)
	ui.Draw(missedShip2)
	missedShipDesc2 := gui.NewText(103, 25, " - No ship there", nil)
	ui.Draw(missedShipDesc2)

	// Setup cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		for {
			// Wait for turn
			isGameOver, err := waitForYourTurn(url, token, ourBoard, txt, &ourStates)
			if err != nil {
				fmt.Println(err)
			}
			if handleGameOver(isGameOver, txt, ui) {
				cancel() // Cancel context if game is over
				break
			}

			// It's our turn
			txt.SetText("It's your turn!")
			txt.SetBgColor(gui.White)
			getCoordinates := true
			for getCoordinates == true {
				alreadyHit := true
				xOurShot, yOurShot := -1, -1
				char := ""

				// Timer Setup
				ticker := time.NewTicker(1 * time.Second)
				timeLeft := 60
				defer ticker.Stop()
				go func() {
					for timeLeft > 0 {
						timeLeft--
						timerTxt.SetText(fmt.Sprintf("Time left: %d", timeLeft))
						<-ticker.C
					}
					// Handle timeout
					ticker.Stop()
					switch timeLeft {
					case 0:
						cancel()
					case -1:
						timerTxt.SetText("")
					}
				}()

				// Get coordinates
				for alreadyHit == true {
					char = enemyBoard.Listen(ctx)
					// If there's a timeout, set game as lost and break
					if char == "" {
						ui.Log("Timed out! You've lost!")
						return
					}
					xOurShot, yOurShot = fieldToInts(char)
					// Ensure coordinates haven't been used yet
					switch enemyStates[xOurShot][yOurShot] {
					case gui.Miss, gui.Hit, gui.Ship:
						txt.SetText("Coordinate already hit! Choose another one!")
						txt.SetBgColor(gui.Red)
					default:
						alreadyHit = false
					}
				}
				// Stop timer
				timeLeft = -1

				// Fire our shot and update board
				ui.Log("Coordinate: %s", char)
				ourHit, sunk, err := fireShot(char, url, token)
				totalShots++
				if ourHit {
					totalHits++
					timeLeft = -2
				}
				accuracyTxt.SetText(fmt.Sprintf("Shot accuracy: %d/%d", totalHits, totalShots))
				getCoordinates = ourHit // Update for next turn
				if err != nil {
					fmt.Println(err)
				}
				updateBoard(xOurShot, yOurShot, enemyBoard, &enemyStates, ourHit)
				if ourHit {
					if sunk == false {
						txt.SetText("Hit!")
						txt.SetBgColor(gui.Green)
					} else {
						txt.SetText("Sunk!")
						txt.SetBgColor(gui.Red)
						shipsLeft[sinkShip(xOurShot, yOurShot, enemyBoard, &enemyStates)]--
						updateLeftShips(shipsLeftTxt, shipsLeft)

						gameStatus, err := getGameStatus(url, token)
						if err != nil {
							fmt.Println(err)
						}
						updateGameOver(&gameStatus, &isGameOver)
						if isGameOver != Nope {
							break // Break if game is over
						}
					}
				}
			}
			if handleGameOver(isGameOver, txt, ui) {
				break // Break if game is over
			}
			txt.SetText("Wait for your enemy...")
			txt.SetBgColor(gui.White)
		}
		ui.Log("Leaving the game...")
		cancel()
	}()

	// Start board and make sure to delete game
	ui.Start(ctx, nil)
	_, err := sendRequest("DELETE", fmt.Sprintf("%s/abandon", url), token, nil)
	if err != nil {
		fmt.Println(err)
	}
}
