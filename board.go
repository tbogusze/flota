package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Board struct {
	Board []string `json:"board"`
}

type BoardInt struct {
	x int
	y int
}

func sendRequest(method, url, token string, body []byte) (*http.Response, error) {
	for i := 0; i < 10; i++ {
		req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
		if err != nil {
			return nil, err
		}

		req.Header.Add("Content-Type", "application/json")
		if token != "" {
			req.Header.Add("X-Auth-Token", token)
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if resp.StatusCode == 503 {
			continue
		}
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
	return nil, fmt.Errorf("too many retries")
}

func getBoard(url, token string) (Board, error) {
	resp, err := sendRequest("GET", url, token, nil)
	if err != nil {
		return Board{}, err
	}
	defer resp.Body.Close()

	var board Board
	err = json.NewDecoder(resp.Body).Decode(&board)
	if err != nil {
		return Board{}, err
	}

	return board, nil
}

func getGameStatus(url, token string) (GameStatus, error) {
	resp, err := sendRequest("GET", url, token, nil)
	if err != nil {
		return GameStatus{}, err
	}
	defer resp.Body.Close()

	var gameStatus GameStatus
	err = json.NewDecoder(resp.Body).Decode(&gameStatus)
	if err != nil {
		return GameStatus{}, err
	}

	return gameStatus, nil
}

func getDescriptions(url, token string) (Descriptions, error) {
	resp, err := sendRequest("GET", fmt.Sprintf("%s/%s", url, "desc"), token, nil)
	if err != nil {
		return Descriptions{}, err
	}
	defer resp.Body.Close()

	var descriptions Descriptions
	err = json.NewDecoder(resp.Body).Decode(&descriptions)
	if err != nil {
		return Descriptions{}, err
	}

	return descriptions, nil
}

func fireShot(coords, url, token string) (bool, bool, error) {
	reqBody, err := json.Marshal(map[string]string{"coord": coords})
	if err != nil {
		return false, false, err
	}

	resp, err := sendRequest("POST", fmt.Sprintf("%s/%s", url, "fire"), token, reqBody)
	if err != nil {
		return false, false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return false, false, fmt.Errorf("Error: HTTP status code %d", resp.StatusCode)
	}

	var result map[string]string
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return false, false, err
	}

	switch result["result"] {
	case "miss":
		return false, false, nil
	case "hit":
		return true, false, nil
	case "sunk":
		return true, true, nil
	default:
		return false, false, fmt.Errorf("Unknown result: %s", result["result"])
	}
}
